--- SubParCars ---
-- The Following is a list of all the vehicles in this pack, formatted for qb-core vehicles.lua. You should be able to copy/paste most of this into your existing file.

--- Tuner Cars (Lots of customization/liveries) ---
{ model = 'roxanne',       name = 'Roxanne',                       brand = 'Annis',           price = 30000,   category = 'sports',         type = 'automobile', shop = 'tuner' },
{ model = 'elegyheritage', name = 'Elegy RH5 Heritage',            brand = 'Annis',           price = 30000,   category = 'sports',         type = 'automobile', shop = 'tuner' },
{ model = 'elegyrh6',      name = 'Elegy RH6',                     brand = 'Annis',           price = 30000,   category = 'sports',         type = 'automobile', shop = 'tuner' },
{ model = 'elegysanshi',   name = 'Elegy San-Shi',                 brand = 'Annis',           price = 30000,   category = 'sports',         type = 'automobile', shop = 'tuner' },
{ model = 'elegyrh7',      name = 'Elegy RH7',                     brand = 'Annis',           price = 30000,   category = 'sports',         type = 'automobile', shop = 'tuner' },
{ model = 'elegyz',        name = 'Elegy Zirconium',               brand = 'Annis',           price = 30000,   category = 'sports',         type = 'automobile', shop = 'tuner' },
{ model = 'rh82',          name = 'Elegy RH8 FR-Works',            brand = 'Annis',           price = 30000,   category = 'sports',         type = 'automobile', shop = 'tuner' },
{ model = 'elegyx',        name = 'Elegy RH8-X',                   brand = 'Annis',           price = 30000,   category = 'sports',         type = 'automobile', shop = 'tuner' },
{ model = 'elegy4',        name = 'Elegy RZ Custom',               brand = 'Annis',           price = 30000,   category = 'sports',         type = 'automobile', shop = 'tuner' },
{ model = 'elegyrh8wide',  name = 'Elegy RH8 Mandem WB',           brand = 'Annis',           price = 30000,   category = 'sports',         type = 'automobile', shop = 'tuner' },

--- Modified Cars (Engine Swaps, Heavily Modified, Drift/Race Builds, etc.) ---
{ model = 'elegy69',       name = 'Engine Swapped Elegy Retro',    brand = 'Annis',           price = 30000,   category = 'muscle',         type = 'automobile', shop = 'mod' },
{ model = 'elegyute',      name = 'Elegy Retro Ute',               brand = 'Annis',           price = 30000,   category = 'muscle',         type = 'automobile', shop = 'mod' },
{ model = 'elegyw',        name = 'Elegy Wagon',                   brand = 'Annis',           price = 30000,   category = 'sedan',          type = 'automobile', shop = 'mod' },
{ model = 'elegy5',        name = 'Elegy RH5 Drift Missle RHD',    brand = 'Annis',           price = 30000,   category = 'sports',         type = 'automobile', shop = 'drift' },
{ model = 'elegy3',        name = 'Elegy RH5 Drift Missle LHD',    brand = 'Annis',           price = 30000,   category = 'sports',         type = 'automobile', shop = 'drift' },
{ model = 'elegyr',        name = 'Elegy RH5 Rally LHD',           brand = 'Annis',           price = 30000,   category = 'sports',         type = 'automobile', shop = 'race' },

--- Regular Cars ---
