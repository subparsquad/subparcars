Citizen.CreateThread(function()
    updatePath = "subparsquad/subparcars" -- your git user/repo path
    resourceName = "SubParCars (" .. GetCurrentResourceName() .. ")" -- the resource name
  
    function splitVersion(version)
        local parts = {}
        for part in version:gmatch("([^.]+)") do
            table.insert(parts, tonumber(part))
        end
        return parts
    end
  
    function compareVersions(curVersion, newVersion)
        for i = 1, math.max(#curVersion, #newVersion) do
            local curPart = curVersion[i] or 0
            local newPart = newVersion[i] or 0
            if curPart < newPart then
                return -1
            elseif curPart > newPart then
                return 1
            end
        end
        return 0
    end
  
    function checkVersion(err, responseText, headers)
        if err ~= 200 then
            print("\nFailed to check version, HTTP error code: " .. err)
            return
        end
  
        local curVersionStr = LoadResourceFile(GetCurrentResourceName(), "version")
        
        if curVersionStr == nil then
            print("\nFailed to load current version, make sure the 'version' file exists in the resource root directory.")
            return
        end
        local curVersion = splitVersion(curVersionStr)
        local newVersion = splitVersion(responseText)
  
        local comparison = compareVersions(curVersion, newVersion)
  
        if comparison == -1 then
            print("\n" .. resourceName .. " is outdated!\nv" .. curVersionStr .. " < v" .. responseText .. "\nGet the latest update at https://gitlab.com/" .. updatePath .. "\n")
        elseif comparison == 1 then
            print("\nHey there time traveler!\nYour copy of " .. resourceName .. " is on v" .. curVersionStr .. ".\nBut v" .. responseText .. " is what is publicly available.\nYou are entering uncharted waters, you have been warned!\n")
        else
            print("\n" .. resourceName .. " is up to date (v".. curVersionStr .."), have fun!\n")
        end
    end
  
    PerformHttpRequest("https://gitlab.com/" .. updatePath .. "/-/raw/main/version", checkVersion, "GET")
  end)
  